# isroreOS设置ipv6的方式：Slaac和NAT

#### 介绍
上海电信64位PD前缀，光猫拨号，istoreOS软路由设置ipv6的两种方法

#### 软件架构
istoreOS，电信只给了64位PD前缀，并且光猫拨号，下面接软路由，实现ipv6


#### 安装教程

1.  Slaac方式
2.  NAT

#### 使用说明
上册---Slaac方式


第一步，先搞定wan6口
1、网络--接口--wan6--编辑
1.1、常规设置
     协议：DHCPv6客户端
         设备：eth0
         开机自动运行：选择
         请求IPv6地址：try
         请求指定长度的IPv6前缀：自动
       
1.2、高级设置
         强制链路：不选
         请求DHCP时发送的客户端ID：空
         使用默认网关：选择
         自动获取DNS服务器：选择
         DNS 权重：空
     使用网关跃点：空
     覆盖 IPv4 路由表：未指定
     覆盖 IPv6 路由表：未指定
     IPv6 源路由：选择
     委托 IPv6 前缀：选择
     IPv6 分配长度：已禁用
     IPv6 前缀过滤器：-- 请选择 --
     IPv6 后缀：::1
     IPv6 优先级：空
     
                          
1.3、防火墙设置
     创建和分配防火墙区域：wan和wan6
     
1.4、DHCP服务器--常规设置
     忽略此接口：不选
     
1.5、DHCP服务器--IPv6设置
     指定的主接口：选择
     RA 服务：中继模式
     DHCPv6 服务：中继模式
     NDP 代理：中继模式
     学习路由：选择
     

第二步，搞定lan口
2、网络--接口--lan--编辑
2.1、常规设置
     协议：静态地址
     设备：br-lan
     开机自动运行：选择
     IPv4 地址：192.168.100.1
     IPv4 子网掩码：255.255.255.0
     IPv4 网关：灰色无法更改
     IPv4 广播地址：灰色无法更改
     IPv6 地址：空
     IPv6 网关：空
     IPv6 路由前缀：空
     
2.2、高级设置
     强制链路：选择
     使用默认网关：选择
     使用自定义的 DNS 服务器：空
     DNS 搜索域名：空
     DNS 权重：空
     使用网关跃点：空
     覆盖 IPv4 路由表：未指定
     覆盖 IPv6 路由表：未指定
     委托 IPv6 前缀：选择
     IPv6 分配长度：已禁用
     IPv6 前缀过滤器：-- 请选择 --
     IPv6 后缀：::1
     IPv6 优先级：空
     
                       
2.3、防火墙设置
     创建和分配防火墙区域：lan
     
2.4、DHCP服务器--常规设置
     忽略此接口：不选
     启动：100
     客户数：150
     租期：infinite
     
2.5、DHCP服务器--高级设置
     动态 DHCP：选择
     强制：不选
     IPv4 子网掩码：灰色无法更改
     DHCP 选项：空
     
2.6、DHCP服务器--IPv6设置
     指定的主接口：不选
     RA 服务：中继模式
     DHCPv6 服务：中继模式
     NDP 代理：中继模式
     学习路由：选择
     NDP 代理从属设备：不选
     

第三步，网络--接口--全局网络选项
     IPv6 ULA 前缀：空
     数据包引导：选择
     

第四步，网络--DHCP/DNS--高级设置
     过滤 IPv6 AAAA 记录：不选
     过滤 IPv4 A 记录：不选
     

第五步，网络--防火墙--自定义规则
# WebDav 6086
ip6tables -A INPUT -p tcp --dport 6086 -j ACCEPT
# Aira2 6881,6882
ip6tables -A INPUT -p tcp    --dport 6881  -j ACCEPT
ip6tables -A INPUT -p udp  --dport 6881  -j ACCEPT
ip6tables -A INPUT -p tcp    --dport 6882  -j ACCEPT
ip6tables -A INPUT -p udp  --dport 6882  -j ACCEPT
# qBittorrent Docker
ip6tables -A INPUT -p tcp    --dport 6883  -j ACCEPT
ip6tables -A INPUT -p udp  --dport 6883  -j ACCEPT
# icmpv6 message go into router itself
ip6tables -I INPUT -p ipv6-nonxt -m length --length 40 -j ACCEPT
ip6tables -I INPUT -p udp --sport 547 --dport 546 -j ACCEPT
ip6tables -I INPUT -p udp --sport 500 --dport 500 -j ACCEPT
ip6tables -I INPUT -p udp --sport 4500 --dport 4500 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 1 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 2 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 3 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 4 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 128 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 129 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 130 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 131 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 132 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 133 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 134 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 135 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 136 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 141 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 142 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 143 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 148 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 149 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 151 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 152 -j ACCEPT
ip6tables -I input_rule -p ipv6-icmp --icmpv6-type 153 -j ACCEPT
# icmpv6 message go into other PC
ip6tables -I FORWARD -p ipv6-nonxt -m length --length 40 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 1 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 2 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 3 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 4 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 128 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 129 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 130 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 131 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 132 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 133 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 134 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 135 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 136 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 141 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 142 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 143 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 148 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 149 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 151 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 152 -j ACCEPT
ip6tables -I forwarding_rule -p ipv6-icmp --icmpv6-type 153 -j ACCEPT

软路由重启（系统--重启）

IPV6联网测试
https://testipv6.cn/
ping -6 www.baidu.com

第六步，其他问题
6.1、如果获得了ipv6地址，缺无法ping通，大概率是以下三个地方
网络--接口--全局网络选项，IPv6 ULA 前缀：空
网络--接口--wan6，IPv6 分配长度：已禁用
网络--接口--lan，IPv6 分配长度：已禁用
原因是纯 relay 模式下客户端不会获取到“IPv6 ULA 前缀”下的地址/路由/邻居信息，当客户端尝试回复从路由器发出的 ULA-prefix 下地址的 neighbor solicitation 时，匹配不到这个 ULA 地址的路由只好放弃。
6.2、软路由iStore，22.03.6版的防火墙配置，是一个通用配置，不适合国内的IPv6环境，icmpv6的消息被防火墙阻挡，需要根据情况打开。
6.3、网络拓扑：电信光猫拨号-->OpenWrt X86电脑（iStore）-->AP。
软件是 iStoreOS 22.03.6 2024012613。日常CPU负载20%以下，8G内存剩下7G以上空闲。
6.4、iStoreOS 页面设置在网络防火墙一节似乎没有很完美，明明页面设置了，后台文件没变化
6.5、两个网关问题
        iStore首页--终端--输入root和密码，查询路由器的默认 IPv6 网关
        ip -6 route | grep default 得到：
        default from 240e:38a:xxx:xxx::/64 via fe80::133:7cc9:xxx dev eth0 proto static metric 512 pref medium  #外网到光猫lan侧
        default from 240e:38a:xxx:xxx::/64 via fe80::1 dev eth0 proto static metric 640 pref medium
        出现此情况，在本模式（SLAAC）下忽略，在IPV6 NAT模式下需要修改若干内容
6.5、openwrt 需要的 ipv6 包：
        odhcp6c                  2022-08-05-7d21e8d8-18，DHCPv6 客户端
        odhcpd-ipv6only      2023-01-02-4a673e1c-2， ipv6版本
         luci-proto-ipv6        git-21.148.48881-79947af    git-23.355.78888-e047387，从 luci Web 界面配置 IPv6
         ip6tables-mod-nat      1.8.7-7
         ip6tables-zz-legacy    1.8.7-7
         kmod-ip6tables           5.10.201-1  并非必须，提供IPv6防火墙
         kmod-nf-ipt6               5.10.201-1
         kmod-ipt-nat6            提供IPv6的NAT支持
6.6、文件 /etc/config/dhcp 如下
config dhcp 'lan'
        option interface 'lan' #端口名称
        option start '100'    #端口号起始--结束
        option limit '150'
        option dhcpv4 'server'
        option ra_management '2' #分配何种地址，0仅SLAAC， 1是SLAAC和DHCP混合， '2'仅DHCP有状态
        option leasetime '12h'  # infinite 租期，v4v6均有效
        option ra 'relay  '         #中继运行模式
        option ndp 'relay'        #中继运行模式
        option dhcpv6 'relay'  #中继运行模式

config dhcp 'wan'
        option interface 'wan'
        option ignore '1'

config dhcp 'wan6'
        option interface 'wan6'
        option master '1'
        option ra 'relay'
        option ndp 'relay'
        option dhcpv6 'relay'
6.7、passwall插件，DNS设置，过滤代理域名 IPv6，要勾选，不对IPV6解析，使得IPV6直接连通，不经过passwall。



下册---NAT方式



1. 查询路由器的默认 IPv6 网关，如果有2个网关，需要执行此步骤，如果就一个FE80::1，这不需要此步
ip -6 route | grep default
default from 240e:38a:1999:xxx::/64 via fe80::133:xxx:xxx:xxx dev eth0 proto static metric 512 pref medium  #外网到光猫lan侧
default from 240e:38a:1999:xxx::/64 via fe80::1 dev eth0 proto static metric 640 pref medium

后面的 print $5 指的就是 第5列的 fe80::133:xxx:xxx:xxx
然后将上面查询得到的默认网关设置为当前 NAT6 模式下的 默认网关 。运行命令如下：

编辑文件  vim /etc/hotplug.d/iface/90-ipv6
#!/bin/sh
[ "$ACTION" = ifup ] || exit 0
GWaddr=`ifconfig | ip -6 route show default | head -n1 | awk '{print $5}'`
route -A inet6 add default gw $GWaddr dev eth0

赋予执行权限
chmod +x /etc/hotplug.d/iface/90-ipv6
 
2. 防火墙配置，添加 /etc/firewall.user  重要
# ipv6 NAT
ip6tables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
ip6tables -A FORWARD -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
ip6tables -A FORWARD -i br-lan -j ACCEPT
# qbittorrent 19422 tcp udp
ip6tables -A INPUT -p tcp --dport 19422 -j ACCEPT
ip6tables -A INPUT -p udp --dport 19422 -j ACCEPT
# WebDav 6086
ip6tables -A INPUT -p tcp --dport 6086 -j ACCEPT
# Aira2 6881,6882
ip6tables -A INPUT -p tcp    --dport 6881  -j ACCEPT
ip6tables -A INPUT -p udp  --dport 6881  -j ACCEPT
ip6tables -A INPUT -p tcp    --dport 6882  -j ACCEPT
ip6tables -A INPUT -p udp  --dport 6882  -j ACCEPT

3. 设置Lan口的 DHCPv6(复制文字时用文字编辑器过渡，防止字体有格式)重要

修改文件 /etc/config/dhcp 如下，文件内容见最后
config dhcp 'lan'
        option interface 'lan'      #端口名称
        option start '100'           #端口号起始--结束
        option limit '150'
        option dhcpv4 'server'
        option leasetime '12h'      # infinite 租期，v4v6均有效
        option ra 'server'             #RA通告服务的运行模式
        option ra_default '2'         # 1是SALLC和DHCP下均通告，2是强制通告
        option dhcpv6 'server'      #DHCP运行模式
        list ra_flags 'managed-config'
        list ra_flags 'other-config'

config dhcp 'wan'
        option interface 'wan'
        option ignore '1'

config dhcp 'wan6'
        option interface 'wan6'
        option master '1'
        option ra 'relay'
        option ndp ''relay'
        option dhcpv6 'relay'


4. 修改网络配置
可直接在 luci 界面--网络--接口--ULA 前缀，修改
config globals 'globals'
        option ula_prefix 'fd00::/64'
        option packet_steering '1'

重启网络服务
/etc/init.d/firewall restart
/etc/init.d/network restart

5.passwall插件，DNS设置，过滤代理域名 IPv6，要勾选，不对IPV6解析，使得IPV6直接连通，不经过passwall。重要

6.openwrt 需要的 ipv6 包，更新重要
odhcp6c                  2022-08-05-7d21e8d8-18，DHCPv6 客户端
odhcpd-ipv6only      2023-01-02-4a673e1c-2， ipv6版本
luci-proto-ipv6        git-21.148.48881-79947af    git-23.355.78888-e047387，从 luci Web 界面配置 IPv6
ip6tables-mod-nat      1.8.7-7
ip6tables-zz-legacy    1.8.7-7
kmod-ip6tables          5.10.201-1  并非必须，提供IPv6防火墙
kmod-nf-ipt6              5.10.201-1
kmod-ipt-nat6            提供IPv6的NAT支持

 
































